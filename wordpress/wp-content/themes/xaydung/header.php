<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Công ty xây dựng Minh Trường</title>
        <meta name="description" content="agilsun">
        <meta name="viewport" content="width=device-width">
        <link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.png" type="image/x-icon">
        <?php wp_head(); ?>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/owl.theme.css">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/owl.transitions.css">

        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/main.css">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/news.css">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/about.css">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/contact.css">

        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/reponsive.css"

</head>
<body>
<div id="container">
    <header>
        <div class="header">
            <section class="container">
                <div class="row head-top">     
                    <div class="col-md-6 telephone">
                        <h2>
                        	<?php echo get_field('telephone_header', 2747 );?>
                        </h2>
                    </div>
                    <div class="col-md-6 search-page">
                        <form role="search" method="get" class="search-form search-box" action="<?php echo home_url( '/' ); ?>">
                                <input type="search" class="search-field search" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" placeholder="Tìm kiếm ... "/>
                            <button type="submit" class="search-submit glyphicon glyphicon-search" value="<?php echo esc_attr_x( '', 'submit button' ) ?>" />
                        </form>
                    </div>

                </div>
                <div class="row head-bot">     
                    <div class="col-md-4 logo">
                        <h1>
                        	<a href="http://localhost/xaydung/wordpress/">
                                 <img src="<?php echo get_field('logo', 2747);?>" alt="image">
                        	</a>
                        </h1>
                    </div>
                    <div class="col-md-8 header-menu">
	                    <div class="main-menu-nav">
	                        <div class="main-menu">
	                            <div class="navbar-header">
	                                <button class="navbar-toggle nav-toggle-menu" data-target=".bs-navbar-collapse" data-toggle="collapse" type="button">
	                                    <i class="glyphicon glyphicon-align-justify"></i>
	                                    <span class="sr-only">
	                                        Toggle navigation
	                                    </span>
	                                    <span class="icon-bar"></span>
	                                    <span class="icon-bar"></span>
	                                    <span class="icon-bar"></span>
	                                </button>
	                            </div>
	                            <nav class="collapse navbar-collapse bs-navbar-collapse nav-collapse" role="navigation" style="padding: 0px;">
	                                <ul class="nav nav-pills main-nav menu-header">  
                                        <?php do_shortcode('[main_menu]'); ?>
	                                </ul> 
	                            </nav>
	                        </div>
	                    </div>															
                	</div>
                </div>
                
            </section>
        </div>
    </header><!--end-header-->