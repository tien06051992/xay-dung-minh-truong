<?php get_header(); ?>
    <section class="container">
	<div class="row main-body">
        <div class="left-content">
        	<!-- Breadcrumb -->
            <div class="row breadcrumb-wrapper">
            	<ul class="breadcrumb">
                  	<li><a href="<?php bloginfo( 'url' ); ?>">HOME</a></li>
                  	<li class="active"><?php printf( __( 'Từ khóa: %s', '' ), get_search_query()); ?></li>
                </ul>
            </div>
            <!-- /Breadcrumb -->
            
            <div class="row category-posts">
                <?php if (have_posts()) : while (have_posts()) : the_post();?>
                <a href="<?php the_permalink(); ?>">
                    <div class="news-detail">
                        <div class="date-time">
                            <h3><?php the_time('j'); ?></h3>
                            <p><?php the_time('F'); ?></p>
                            <h5><?php the_time('Y'); ?></h5>
                        </div>
                        <div class="image-news">
                            <?php the_post_thumbnail('img-home-news'); ?>
                        </div>
                        <div class="content-news">
                            <h5><?php the_title(); ?></h5>
                            <p><?php agilsun_get_excerpt(agilsun_excerptlength_cat_new, agilsun_excerptmore); ?></p>
                        </div>
                    </div>
                </a>

            <?php endwhile; endif; ?>
                
                <?php if (function_exists("pagination")) { pagination(); } ?>
            </div>
        </div>
        
    </div>
</section>
<?php get_footer(); ?>