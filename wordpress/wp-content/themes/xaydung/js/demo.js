// external js:
// http://isotope.metafizzy.co/beta/isotope.pkgd.js
// http://isotope.metafizzy.co/beta/bower_components/isotope-fit-columns/fit-columns.js

$( function() {
  
  $('.isotope').isotope({
    layoutMode: 'fitColumns',
    itemSelector: '.item'
  });
});
