
<?php
/*
Template Name: Template contact
*/
get_header();
?>


    <div id="main" role="main">
        <section class="container">
            <div class="maps">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3919.446322635791!2d106.65622560000001!3d10.777087800000002!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752ec6e636f24f%3A0x23e19b2a7f8a3786!2zMzE5IEzDvSBUaMaw4budbmcgS2nhu4d0LCBwaMaw4budbmcgMTQsIFF14bqtbiAxMSwgSOG7kyBDaMOtIE1pbmgsIFZpZXRuYW0!5e0!3m2!1sen!2s!4v1425375602695" width="600" height="450" frameborder="0" style="border:0"></iframe>
                <span><img src="<?php echo get_template_directory_uri(); ?>/img/boder-maps.png"></span>
            </div>
            <div class="detail-info-contact info-contact">
             <form action="https://docs.google.com/forms/d/1mzU_ko8SZrF4TLhzjTV5nOh63uPbsJ6cqgQyzfozaBA/formResponse" method="POST" target="_blank" class="form-contact">
                <div class="title-info-contact">
                    <p>Hãy để chúng tôi giải đáp thắc mắc của bạn</p>
                </div>
                <div class="contact-form">
                    <div class="row form-top">
                        <div class="col-md-6">
                            <input type="text" required name="entry.621171733" value="" class="ss-q-short valid" id="entry_621171733" dir="auto" aria-label="Họ tên  " title="" placeholder="Họ tên">
                        </div>
                        <div class="col-md-6">
                            <input type="text" required name="entry.837142045" value="" class="ss-q-short valid" id="entry_837142045" dir="auto" aria-label="Email  " title="" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-message">
                        <div class="col-md-9">
                           <textarea required name="entry.15653459" rows="8" cols="0" class="ss-q-long valid" id="entry_15653459" dir="auto" aria-label="Nội dung  " placeholder="Nội dung"></textarea>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" name="submit" id="ss-submit" class="btn btn-send-messenger">Gửi</button>  
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php get_footer(); ?>