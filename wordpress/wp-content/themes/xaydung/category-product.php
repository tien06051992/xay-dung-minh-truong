
<?php
/*
Template Name: Template about
*/
get_header();
?>

<?php get_header(); ?>
	<div id="main" role="main">
        <section class="container">
            <?php 
                    $child_cat = ags_get_child_category(213); //213 is id of product category
                     { 
                    ?>
            <div class="product">
                <?php $image = get_field( 'feature_image', 'category_' . 217); ?>
                 <img src="<?php echo $image; ?>" alt="">
                <p><?php echo category_description(); ?></p>
            </div>

            <?php
                    }
                    ?>
            <div class="product-typify">
                <div class="title-product-typify">
                    <p><?php echo get_cat_name( 216 ); // 216 is id of category san pham tieu bieu?></p>
                </div>
                <div class="info-product-typify">
                <?php 
                    $count = 0;
                    query_posts( array ( 'category_name' => 'san-pham-tieu-bieu', 'posts_per_page' => 6)); ?>
                    <?php 
                     while (have_posts()) : the_post(); ?>
                    <div class="detail-product-typify">
                         <a href="" type="button" class="btn btn-product" data-toggle="modal" data-target=".product-news<?php echo $count ?>">
                            <?php the_post_thumbnail('image-product'); ?>
                            <h5><?php the_title(); ?></h5>
                        </a>

                        <div class="modal fade bs-example-modal-lg product-news<?php echo $count ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel1" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                               <?php the_post_thumbnail('img-product-about'); ?>
                               <p><?php the_content(); ?></p>
                            </div>
                          </div>
                        </div>



                    </div>
                     <?php $count++; endwhile; ?> 
                    <?php wp_reset_query(); ?>

                </div>
                    
                </div>

                <div class="product-typify-bot">
                <div class="title-product-typify">
                    <p><?php echo get_cat_name( 217 ); // 217 is id of category san pham tieu bieu?></p>
                </div>
                <div class="info-product-typify">
                <?php 
                    $count = 0;
                    query_posts( array ( 'category_name' => 'cong-trinh-tieu-bieu')); ?>
                    <?php 
                     while (have_posts()) : the_post(); ?>
                    <div class="detail-product-typify">
                         <a href="" type="button" class="btn btn-product" data-toggle="modal" data-target=".product-news-typify<?php echo $count ?>">
                            <?php the_post_thumbnail('image-product'); ?>
                            <h5><?php the_title(); ?></h5>
                        </a>

                        <div class="modal fade bs-example-modal-lg product-news-typify<?php echo $count ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel1" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                               <?php the_post_thumbnail('img-product-about'); ?>
                                <p><?php the_content(); ?></p>
                            </div>
                          </div>
                        </div>
                    </div>
                     <?php $count++; endwhile; ?> 
                    <?php wp_reset_query(); ?>

                </div>
                    
                </div>

                <div class="product-typify-kh">
                <div class="title-product-typify">
                    <p><?php echo get_cat_name( 218 ); // 217 is id of category san pham tieu bieu?></p>
                </div>
                <div class="info-product-typify-kh">
                <?php 
                    $count = 0;
                    query_posts( array ( 'category_name' => 'khach-hang')); ?>
                    <?php 
                     while (have_posts()) : the_post(); ?>
                    <div class="detail-product-typify">
                            <?php the_post_thumbnail('image-product'); ?>
                    </div>
                     <?php $count++; endwhile; ?> 
                    <?php wp_reset_query(); ?>

                </div>
                    
                </div>


           
        </section>
        
    </div>

<?php get_footer(); ?>