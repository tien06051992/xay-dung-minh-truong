<?php get_header(); ?>
	<div id="main" role="main">
        <section class="container">
            <div class="home-banner">
                <div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <?php 
                    $count = 0;
                    query_posts( array ( 'category_name' => 'product-banner')); ?>
                  <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic1" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic1" data-slide-to="2"></li>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                
                    <?php 
                     while (have_posts()) : the_post(); ?>
                    <div class="item <?php echo ($count == 0) ? 'active' : ''?>">
                        <?php the_post_thumbnail('img-banner'); ?>
                        
                      <div class="title-banner">
                        <p><?php agilsun_get_excerpt(agilsun_excerptlength_cat, agilsun_excerptmore); ?></p> 
                        <a class="pull-right" href="<?php echo get_category_link(215); //214 is id of solution category ?>">Liên hệ</a>
                      </div>
                    </div>
                    <?php $count++; endwhile; ?> 
                    <?php wp_reset_query(); ?>
                  </div>
                </div>
                
            </div>

            <div class="introduce">
            <?php 
                query_posts("page_id=2783"); 
                    if ( have_posts() ) : the_post(); ?>
            	<div class="title-introduce">
            		<h5><?php the_title(); ?></h5>
            	</div>
            	<div class="content-introduce">
                    <div class="content-about">
                    <p><?php the_content(); ?></p>
                    </div>
                   
                </div>
                <button id="btn-read-more-about-content" class="pull-right">Xem thêm</button>
            <?php endif; wp_reset_query(); ?>
            </div>
        </section>
        <div class="infomation-product">
             <?php query_posts( array ( 'category_name' => 'san-pham')); ?>
                    <div id="owl-list-product">
                      <?php while (have_posts()) : the_post();
                        $postid = get_the_ID(); ?>
                        <div class="item ">
                            <?php the_post_thumbnail('img-product'); ?>
                        </div>
                        <?php endwhile;?> 
                    <?php wp_reset_query(); ?>
                  </div>
        </div>
        <section class="container">
        	<div class="infomation-news">
        		<div class="title-info-news">
        			<?php echo get_cat_name( 214 ); // 214 is id of category news?>
        		</div>
                <div class="list-info-news">
                <?php 
                	$count = 0;
                    query_posts( array ( 'category_name' => 'tin-tuc', 'posts_per_page' => 3)); ?>
                    <?php 
                     while (have_posts()) : the_post(); ?>
                     <a href="<?php the_permalink(); ?>">
	                    <div class="info-news">
	                        <?php the_post_thumbnail('img-home-news'); ?>
	                        <div class="content-info-news">
	                            <h5><?php the_title(); ?></h5>
	                            <p><?php the_time('F, j, Y'); ?></p>
	                        </div>
	                    </div>
                    </a>
                    <?php $count++; endwhile; ?> 
                    <?php wp_reset_query(); ?>
                </div>
        		<div class="see-more">
                    <a class="pull-right" href="<?php echo get_category_link(214); //214 is id of solution category ?>">Xem thêm</a>
                </div>
        	</div>
        </section>
    </div>

<?php get_footer(); ?>