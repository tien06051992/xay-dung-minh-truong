 <div class="footer">
        <section class="container">
            <div class="row info-contact">
                <div class="col-md-2 menu-footer">
                    <nav class="collapse navbar-collapse bs-navbar-collapse nav-collapse" role="navigation" style="padding: 0px;">
                        <ul class="nav nav-pills main-nav menu-header menu-footer">  
                            <?php do_shortcode('[main_menu]'); ?>
                        </ul> 
                    </nav>
                </div>
                <div class="col-md-6 detail-contact">
                    <p>193 Xuân Hồng, Phường 13, Quận Tân Bình, Thành phố Hồ Chí Minh, Việt Nam</p>
                    <div class="telephone">
                        <p>Điện thoại: </p>
                        <div class="phone-number">
                            <p>098 504 72 17</p><br>
                            <p>098 504 72 17</p>
                        </div>
                    </div>
                    <p>Email: minhtruong@info.com</p>
                    <div class="search-page">
                        <form role="search" method="get" class="search-form search-box" action="<?php echo home_url( '/' ); ?>">
                                <input type="search" class="search-field search" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" placeholder="Tìm kiếm ... "/>
                            <button type="submit" class="search-submit glyphicon glyphicon-search" value="<?php echo esc_attr_x( '', 'submit button' ) ?>" />
                        </form>
                    </div>
                </div>
                <div class="col-md-4 detail-product">
                <?php 
                    $count = 0;
                    query_posts( array ( 'category_name' => 'san-pham' , 'posts_per_page' => 9)); ?>
                    <?php 
                     while (have_posts()) : the_post(); ?>
                    <div class="image-product">
                         <a href="" type="button" class="btn btn-product" data-toggle="modal" data-target=".bs-example-modal-lg<?php echo $count; ?>">
                            <?php the_post_thumbnail('img-product-footer'); ?>
                        </a>

                        <div class="modal fade bs-example-modal-lg<?php echo $count ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel1" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                               <?php the_post_thumbnail('img-product'); ?>
                               <p><?php the_content(); ?></p>
                            </div>
                          </div>
                        </div>



                    </div>
                     <?php $count++; endwhile; ?> 
                    <?php wp_reset_query(); ?>

                </div>
            </div>
        </section>
        <div class="footer-bot">
            <p>MinhTruong © 2015</p>
            <h5>Design by <a href="http://agilsun.com/">Agilsun</a></h5>
        </div>
    </div>

</div>
        
<?php wp_footer(); ?>
     <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-1.11.0.min.js"></script>
     <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
     <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/js/owl.carousel.min.js"></script>

        <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/bootstrap.min.js"></script>

        <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
        
</body>

</html>