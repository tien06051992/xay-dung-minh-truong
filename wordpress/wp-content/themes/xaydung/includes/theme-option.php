<?php

	/*add_action( 'admin_head', 'admin_css' );
	function admin_css()
	{
	echo '<link rel="stylesheet" type="text/css" href="'. get_bloginfo("stylesheet_directory") . '/css/bootstrap.min.css">';
	echo '<link rel="stylesheet" type="text/css" href="'. get_bloginfo("stylesheet_directory") . '/css/bootstrap-theme.min.css">';
	}*/
	/**
   * Add Theme Option Menu
   **/

	add_action('admin_menu','wtmse_menu_page');
	function wtmse_menu_page() {
		add_theme_page('WTM SE Theme Option','Theme Option','manage_options','wtmse-theme-option','wtmse_setting_page');
	}

	add_action('admin_init','origin_register_setting');
	function origin_register_setting() {
    register_setting('origin-group','origin-num-slides');
    register_setting('origin-group','origin-home-cat');
    register_setting('origin-group','origin-facebook');
	}


	
	function wtmse_setting_page() { ?>
		<div class="wrap">
	        <?php screen_icon(); ?>
	        <h2>Origin Setting Page</h2>
	        <form id="origin_setting" method="post" action="options.php">
	        <?php settings_fields('origin-group'); ?>
	            <table class="table">
	                <tr valign="top">
	                    <th scope="row">Contact WTMSE</th>
	                    <td><a target="_blank" href="https://docs.google.com/spreadsheets/d/1Wqm7St_o4fXtfTE_gzhpQlvLgFWM9TGWiStSSCtKiXQ/edit#gid=1771738080">See list contact WTM SE</a></td>
	                </tr>
	                <tr valign="top">
	                    <th scope="row">List register email</th>
	                    <td><a target="_blank" href="https://docs.google.com/spreadsheets/d/1F4AhBlxZyQbBP15GIo2ngTZ-U93ZLQGK8u0L6yMx-fU/edit#gid=1598110914">See list register email news letter</a></td>
	                </tr>
	            </table>
	        </form>
    	</div>
<?php
	}
?>