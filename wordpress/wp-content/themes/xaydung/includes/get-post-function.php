<?php

/**
 * Query custom get post
 */
function agilsun_query($catogery, $numpost) {
    query_posts(array('category_name' => $catogery, // choose catogery which u want to get
        'posts_per_page' => $numpost, //max num post was got
                )
    );
}

/**
 * Query get all post with cat slug
 */
function agilsun_query_get_all_post($catslug, $numpost) {
    query_posts(array('category_name' => $catslug,
        'posts_per_page' => $numpost));
}

/**
 * Agilsun get post top content in home
 */
function agilsun_get_post_testimonial($catogery, $numpost) {
    agilsun_query($catogery, $numpost);
    while (have_posts()) : the_post();
        ?>
            <div class="content-about-solution">
                <?php the_post_thumbnail('testimonial-thumb'); ?>
                <?php the_content(); ?>
                <?php the_title(); ?>
            </div>
        <?php
    endwhile;
    wp_reset_query();
}

/**
 * Agilsun get gallery top content in home
 */
function agilsun_get_post_gallery($catogery, $numpost) {
    agilsun_query($catogery, $numpost);
    while (have_posts()) : the_post();
        ?>
        <a href="<?php the_permalink(); ?>">
            <div class="content-about-solution">
                <?php the_post_thumbnail('img-gallery-feature'); ?>
            </div>
        </a>
        <?php
    endwhile;
    wp_reset_query();
}

/**
 * Agilsun get post news content in home
 */
function agilsun_get_post_news_sidebar($catogery, $numpost) {
    //agilsun_query($catogery, $numpost);
    query_posts( array( 'category_name' => $catogery, 'posts_per_page' => $numpost, 'meta_key' => 'post_views_count','orderby' => 'meta_value_num', 'order' =>'desc') );
    while (have_posts()) : the_post();
        ?>
        <a href="<?php the_permalink(); ?>">
            <div class="view-more">
                <?php the_post_thumbnail('img-sidebar-news'); ?>
                <p><?php the_title(); ?></p>
            </div>
        </a>
        <?php
    endwhile;
    wp_reset_query();
}


/**
 * Get news post on home
 */
function agilsun_get_post_news_on_home($slug, $numpost) {
    agilsun_query_get_all_post($slug, $numpost);
    while (have_posts()) : the_post();
        ?>
        <div class="home-content-right-news">
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('img-category-thumb'); ?>
                <p><?php echo get_the_title(); ?></p>
            </a>
        </div>
        <?php
    endwhile;
    wp_reset_query();
}

/**
 * Agilsun get post news slide
 */
function agilsun_get_post_news_slide($catogery, $numpost) {
    agilsun_query_get_all_post($catogery, $numpost);
    ?><ul class="slides"><?php
    while (have_posts()) : the_post();
        ?>      <li class="m-thumb entry-image"><a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail('msize'); ?>
                <p><?php echo the_title(); ?></p></a>
            </li>
            <?php
        endwhile;
        wp_reset_query();
        ?></ul><?php
}

/**
 * lấy tất cả post của category hiện tại
 */
function agilsun_get_post_cat() {
    while (have_posts()) : the_post();
        ?>
        <li>
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('ssize'); ?>
                <div class="excerpt">
                <h3><?php the_title(); ?></h3>
                <p><?php agilsun_excerpt("agilsun_excerptlength_cat","agilsun_excerptmore"); ?></p>
                </div>
            </a>
        </li>
        <?php
    endwhile;
    wp_reset_query();
}


/**
 * Count facebook comments
 */
function agilsun_fb_comment_count($url)
{
  $json = json_decode(file_get_contents('https://graph.facebook.com/?ids=' . $url));
  return ($json->$url->comments) ? $json->$url->comments : 0;
}

/**
 * lấy tên category hiện tại
 */
function agilsun_get_current_cat_name() {
    if (is_category()) {
        $cat = get_query_var('cat');
        $yourcat = get_category($cat);
        agilsun_get_catogery_name($yourcat->slug);
    }
}

/**
 * lấy post của category dịch vụ bằng slug
 */
function agilsun_get_post_cat_by_slug($slug, $numpost) {
    agilsun_query_get_all_post($slug, $numpost);
    while (have_posts()) : the_post();
        ?>
        <li>
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('msize'); ?>
                <p><?php the_title(); ?></p>
            </a>
        </li>
        <?php
    endwhile;
    wp_reset_query();
}

/**
 * Get catagory name by slug
 */
function agilsun_get_catogery_name($slug) {
    $catt = get_category_by_slug($slug);
    echo $catt->name;
}

/**
 * Get permalink catagory by slug
 */
function agilsun_get_permalink_catogery($slug) {
    $catt = get_category_by_slug($slug);
    echo get_category_link($catt->term_id);
}

/**
 * Get permalink page by slug
 */
function agilsun_get_permalink_page($page_title) {
    $page = get_page_by_title($page_title);
    echo get_page_link($page->ID);
}


add_post_type_support('page', 'excerpt');

function agilsun_get_page_excerpt($id) {

    query_posts("page_id=$id");
    if ( have_posts() ) : the_post();
        the_excerpt(); 
    endif; 
    wp_reset_query();

}

function agilsun_get_page_content($id) {

    query_posts("page_id=$id");
    if ( have_posts() ) : the_post();
        the_content(); 
    endif; 
    wp_reset_query();

}

function agilsun_get_the_slug() {
global $post;
return $post->post_name;
}

function agilsun_the_slug() {
echo agilsun_get_the_slug();
}


/**
 * Excerpt length for Education
 */
function agilsun_excerptlength_edu($length) {
    return 30;
};


/**
 * Excerpt length for Tour
 */
function agilsun_excerptlength_solution($length) {
    return 30;
};

function agilsun_excerptlength_page_about($length) {
    return 100;
};


/**
 * Excerpt length for Index
 */
function agilsun_excerptlength_cat($length) {
    return 10;
};

function agilsun_excerptlength_cat_product($length) {
    return 5;
};


function agilsun_excerptlength_cat_new($length) {
    return 20;
};

/**
 * Excerpt more...
 */
function agilsun_excerptmore($more) {
    return '...';
};

/**
 * Excerpt no more
 */
function agilsun_excerptnomore($more) {
    return '';
};

/**
 * Excerpt length
 */
function agilsun_get_excerpt($length_callback = '', $more_callback = '') {
     global $post;
     
     if(function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
     }
     
     if(function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
     }
     
     $output = get_the_excerpt();
     $output = apply_filters('wptexturize', $output);
     $output = apply_filters('convert_chars', $output);
     
     echo $output;
};



function ags_get_child_category($id) {
    $categories=get_categories(
    array( 'child_of' => $id,
            'orderby ' => 'ID',
            'order' => 'ASC',
            'hide_empty' => 0,
         )
    );
    return $categories;
}

/**
 *  Get current category id
 */
function agilsun_get_current_cat_id() {
    if (is_category()) {
        $cat = get_query_var('cat');
        return $cat;
    }
}

/**
 *  Get current category id
 */
function agilsun_is_current_cat_id($id) {
    if (is_category()) {
        $cat = get_query_var('cat');
        if($cat == $id) {
            return true;
        }
    }
    return false;
}

/**
 *  Get field by language
 */

function WTMSE_get_field_multilangue($fieldname_en,$fieldname_jp,$id) {
    $current_language = qtrans_getLanguage();
    if($current_language == 'en') {
        echo get_field($fieldname_en,$id);
    }elseif ($current_language == 'sven') {
     echo get_field($fieldname_jp,$id);
    } 
    else {
        echo get_field($fieldname_jp,$id);
    }
}

/**
 *  Get field cat by language
 */
function WTMSE_get_field_cat_multilangue($fieldname_en,$fieldname_vn,$fieldname_sv,$id) {
    $current_language = qtrans_getLanguage();
    if($current_language == 'en') {
        echo get_field($fieldname_en,'category_' . $id);
    }elseif ($current_language == 'vi') {
     echo get_field($fieldname_vn,'category_' . $id);
    } 
    else {
        echo get_field($fieldname_sv,'category_' . $id);
    }
}

// function to display number of posts.
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

// function to count views.
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}


// Add it to a column in WP-Admin
add_filter('manage_posts_columns', 'posts_column_views');
add_action('manage_posts_custom_column', 'posts_custom_column_views',5,2);

function posts_column_views($defaults){
    $defaults['post_views'] = __('Views');
    return $defaults;
}

function posts_custom_column_views($column_name, $id){
  if($column_name === 'post_views'){
        echo getPostViews(get_the_ID());
    }
}

/**
 *  Custom facebook share button
 */

function agilsun_custom_button_share_fb($class,$src_img,$url) {
    ?>
    <div class="<?php echo $class ?>">
        <a class="facebook" target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=300')" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>">
            <img src="<?php echo $src_img; ?>" alt="image">
        </a>
    </div>
    <?php
}

/**
 *  Custom twitter share button
 */

function agilsun_custom_button_share_tw($class,$src_img, $url) {
    ?>
    <div class="<?php echo $class ?>">
        <a class="twitter popup" target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=300')"  href="http://twitter.com/share"> 
            <img src="<?php echo $src_img; ?>" alt="image">
        </a>
    </div>
    <?php
}


/**
 *  Custom google plus share button
 */

function agilsun_custom_button_share_gg($class,$src_img, $url) {
    ?>
    <div class="<?php echo $class ?>">
        <a class="google popup" target="_blank" onclick="return !window.open(this.href, 'google', 'width=640,height=300')"  href="https://plus.google.com/"> 
            <img src="<?php echo $src_img; ?>" alt="image">
        </a>
    </div>
    <?php
}

/**
 *  Custom linkedin share button
 */

function agilsun_custom_button_share_linkedin($class,$src_img, $url) {
    ?>
    <div class="<?php echo $class ?>">
        <a class="google popup" target="_blank" onclick="return !window.open(this.href, 'linkedin', 'width=640,height=300')"  href="https://www.linkedin.com/"> 
            <img src="<?php echo $src_img; ?>" alt="image">
        </a>
    </div>
    <?php
}