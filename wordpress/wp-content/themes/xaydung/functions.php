<?php

include_once( 'includes/theme-option.php' );
include_once( 'includes/get-post-function.php' );
// include_once( 'includes/remove-dashboard-items.php' );

/**
 * Setup Images Size
 */
function agilsun_imagesize() {
    add_image_size('img-banner', 940, 396, true); //banner
    add_image_size('img-home-news', 289, 175, true); // home page-news
    add_image_size('img-product', 411, 297, true); // product home
    add_image_size('image-product', 300, 166, true); // image product
    add_image_size('img-product-footer', 81, 75, true ); // img detail category new
    add_image_size('img-product-about', 390, 250, true ); // img product about
    // add_image_size('img-news-feature', 289, 146, true ); // img detail category new
    // add_image_size('img-sidebar-news', 251, 146, true );    // image post for feature image  on sidebar news
    // add_image_size('img-slider', 275, 275, true );    // image post for feature image  on sidebar news
    // add_image_size('img-post-widgets', 119, 70, true );    // image post widgets  on sidebar
    // add_image_size('img-post-portfolio', 297, 174, true );    // image post portfolio
    // add_image_size('img-blog', 200, 130, true );    // image post blog
    

    add_theme_support('post-thumbnails');
}
add_action('after_setup_theme', 'agilsun_imagesize');

/**
 * Menu Locations
 */

if (function_exists('wp_nav_menu')) {
    function agilsun_wp_my_menus() {
        register_nav_menus(array(
                'main_nav' => __('Main navigation menu'),
                // 'about_sub_menu' => __('About sub navigation menu')
        ));
    }
    add_action('init', 'agilsun_wp_my_menus');
}

function get_main_menu() {
     wp_nav_menu( array(
                    'theme_location'  => 'main_nav',
                    'menu'            => '',
                    'container'       => '',
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'      => '',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => '',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '%3$s',
                    'depth'           => 0,
                    'walker'          => ''
                    ));
}

add_shortcode( 'main_menu' , 'get_main_menu' );

/**
 * Menu about
 **/

// function get_about_sub_menu() {
//      wp_nav_menu( array(
//                     'theme_location'  => 'about_sub_menu',
//                     'menu'            => '',
//                     'container'       => '',
//                     'container_class' => '',
//                     'container_id'    => '',
//                     'menu_class'      => '',
//                     'menu_id'         => '',
//                     'echo'            => true,
//                     'fallback_cb'     => '',
//                     'before'          => '',
//                     'after'           => '',
//                     'link_before'     => '',
//                     'link_after'      => '',
//                     'items_wrap'      => '%3$s',
//                     'depth'           => 0,
//                     'walker'          => ''
//                     ));
// }

// add_shortcode( 'about_sub_menu' , 'get_about_sub_menu' );

/**
 * Register Language Chooser
 */
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => __('Language Chooser', ''),
        'id' => 'language-chooser',
        'description' => 'Language chooser on top header',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
}

/**
 * Register news sidebar
 */
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => __('News Sibar', ''),
        'id' => 'news-sibar',
        'description' => 'News Sibar On News Category',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
}


/**
 * Pagination
 */
// Pagination

  function pagination($pages = '', $range = 4) {
    $showitems = ($range * 2) + 1;

    global $paged;

    if(empty($paged)) $paged = 1;

    if($pages == '') {
      global $wp_query;
      $pages = $wp_query->max_num_pages;
      if(!$pages) {
         $pages = 1;
      }
    }

    if(1 != $pages) {
      echo '<div class="pagination-tt clearfix">';

      echo '<ul class="pagination">';

      if ($paged > 1) {
      // if (qtrans_getLanguage() == 'en') { // English ver
       echo '<li class="pagination-prev"><a href="' . get_pagenum_link($paged - 1) . '" class="pagenav">Prev</a> </li>';
        }
        else {
        echo '<li class="pagination-prev"><a href="' . get_pagenum_link($paged - 1) . '" class="pagenav">Trước</a> </li>';
        }
      }


      for ($i=1; $i <= $pages; $i++) {
        if (1 != $pages &&( !($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems )) {
          echo ($paged == $i) ? "<li><span>" . $i . "</span></li>" : "<li><a href=\"".get_pagenum_link($i)."\">" . $i . "</a></li>";
        }
      }

      // if ($paged < $pages) {
      //   if (qtrans_getLanguage() == 'en') {
      //  echo '<li class="pagination-next"><a href="' . get_pagenum_link($paged + 1) . '" class="pagenav">Next</a> </li>';
      //   }
      //   else {
      //     echo '<li class="pagination-next"><a href="' . get_pagenum_link($paged + 1) . '" class="pagenav">Sau</a> </li>';

      //   }
      // }
      echo '</ul>';

      echo "</div>\n";
     }
  // }

  function my_search_form( $form ) {
        $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
        <div><label class="screen-reader-text" for="s">' . __( 'Search for:' ) . '</label>
        <input type="text" value="' . get_search_query() . '" name="s" id="s" />
        <input type="submit" id="searchsubmit" value="'. esc_attr__( 'Search' ) .'" />
        </div>
        </form>';

        return $form;
  }

add_filter( 'get_search_form', 'my_search_form' );
  
?>