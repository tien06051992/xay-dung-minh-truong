<?php
/*
Template Name: template-detail
*/
?>
<?php  get_header();
if (have_posts()) : the_post();
$post_id = get_the_ID();
?>
	<div id="main" role="main">
        <div class="news-info">
            <section class="container">
                <a href="<?php the_permalink(); ?>">
                    <div class="news-detail">
                        <div class="date-time">
                            <h3><?php the_time('j'); ?></h3>
                            <p><?php the_time('F'); ?></p>
                            <h5><?php the_time('Y'); ?></h5>
                        </div>
                        <div class="image-news">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <div class="content-news">
                            <h5><?php the_title(); ?></h5>
                            <p><?php the_content(); ?></p>
                        </div>
                    </div>
                </a>
            </section>
        </div>
    </div>

    <?php endif; ?>
<?php get_footer(); ?>