
<?php
/*
Template Name: Template News
*/
get_header();
?>

<?php get_header(); ?>
	<div id="main" role="main">
        <div class="news-info">
            <section class="container">
            <?php while (have_posts()) : the_post();?>
                <a href="<?php the_permalink(); ?>">
                    <div class="news-detail">
                        <div class="date-time">
                            <h3><?php the_time('j'); ?></h3>
                            <p><?php the_time('F'); ?></p>
                            <h5><?php the_time('Y'); ?></h5>
                        </div>
                        <div class="image-news">
                            <?php the_post_thumbnail('img-home-news'); ?>
                        </div>
                        <div class="content-news">
                            <h5><?php the_title(); ?></h5>
                            <p><?php agilsun_get_excerpt(agilsun_excerptlength_cat_new, agilsun_excerptmore); ?></p>
                        </div>
                    </div>
                </a>

            <?php endwhile;?> 
                    <?php wp_reset_query(); ?>
                <?php if (function_exists("pagination")) { pagination(); } ?>
            </section>
        </div>
    </div>

<?php get_footer(); ?>